<?php
/**
 * Created by PhpStorm.
 * User: albert
 * Date: 4/15/2018
 * Time: 11:08 AM
 */

namespace Ddd\Domain;


interface ClientRepository
{
    public function save(Client $client);

    public function findById(ClientId $clientId);
}