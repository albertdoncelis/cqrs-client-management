<?php

namespace spec\Ddd\Application;


use Ddd\Application\ClientSignUpCommand;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class UserRequestSpec
 * @package spec\Ddd\Application\Service\Client
 *
 * @mixin \Ddd\Application\Service\ClientSignUpCommand
 */
class ClientSignUpCommandSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ClientSignUpCommand::class);
    }
}
