<?php

namespace spec\Ddd\Application\Handler;

use Ddd\Application\Handler\ClientSignUpCommandHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ClientSignUpCommandHandlerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ClientSignUpCommandHandler::class);
    }
}
