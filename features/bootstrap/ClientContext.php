<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class ClientContext implements Context
{
    private $employee;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }


    /**
     * @Given I am logged in as :employee
     */
    public function iAmLoggedInAs($employee)
    {
       $this->employee['name'] = $employee;
       $this->employee['id'] = md5($employee);
    }

    /**
     * @Given I need to register the client:
     */
    public function iNeedToRegisterTheClient(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @When I register his account
     */
    public function iRegisterHisAccount()
    {
        throw new PendingException();
    }

    /**
     * @Then I should see the client in the database with Id:
     */
    public function iShouldSeeTheClientInTheDatabaseWithId(TableNode $table)
    {
        throw new PendingException();
    }

    /**
     * @Then I should see a throw exception :errorMessage
     */
    public function iShouldSeeAThrowException($errorMessage)
    {
        throw new PendingException();
    }

}
