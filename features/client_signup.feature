Feature: Client Signup
  In an Employee
  I need to register the client using our cms

  Scenario: Client SignUp by employee
    Given I am logged in as "Juan Dela Cruz"
    And I need to register the client:
    | username | password    |
    | demo1    | password123 |
    When I register his account
    Then I should see the client in the database with Id:
    | username | password    |
    | demo1    | password123 |

  Scenario: Client SignUp by employee throw exception when the password was empty
    Given I am logged in as "Juan Dela Cruz"
    And I need to register the client:
    | username | password |
    | demo1    |          |
    When I register his account
    Then I should see a throw exception "password is required"

  Scenario: Client SignUp by employee throw exception when the username was empty
    Given I am logged in as "Juan Dela Cruz"
    And I need to register the client:
      | username | password    |
      |          | password123 |
    When I register his account
    Then I should see a throw exception "username is required"

